# Plot ICE spots

plot_ICE_spot.py is a (old) python script. It takes as an input an pseudo-tabular file.
Some of these can be found in the `Data/` folder.

Here each input file contain the ICEs for a given species. For each ICE, there is the information on its spot, which is the set of intervals between the two core genes flanking the conjugative systems.

# How to use it

To use it:

    ./plot_ICE_spot.py Data/All_data-xxxx.txt

The window can be clicked:

- left click on gene: print information for that genes
- left click on any x-axis: erase all gene informations
- right click on gene: select the gene as a first and last gene delimiting the ICE
- shift + right-click: remove selected genes in the corresponding ICE.

Beware to click 2 times in the same ICE otherwise the limits will be mixed between ICEs (in case of odd number of right-clicks).

If you click 4 times in the same ICE, it will define 2 ICEs in tandem. (Or any even number of right click will define half as many ICE)

## Tutorial

In the file `Tutorial_ICE.ipynb`, one can find how to detect conjugative systems in different chromosomes (or ICEs), and how to delimit them using comparative genomics. (You can visualize it [here](https://github.com/gem-pasteur/Macsyfinder_models/blob/master/models/Conjugation/Tutorial_ICE.ipynb))

# Input files
Each input file contain a header line (not used in the script), starting with a `#`
Then, it contains the information for one or more spots. For each spot, the information is represented as follow:

    #header
    \n
    ICE_ID
    genome1&2 genome2&2 genome3&50 genome4&2 #list of genome ID with the number of gene in between the 2 core genes (included)
    <Table with genes from the genomes with the ICE in columns, see below for description>
    0.4 0.45 0.43 .... #GC% values along the spot.
    \n

There can be as many block like this in the file. They will be plotted 4 at a time.

The table has the following columns:

1. Gene_id
1. Annotation
1. Start
1. End
1. Gene orientation (1/-1)
1. Replicon orientation (1/-1) # compare to the other genome of the species, is this replicon sequenced on the same strand or not ? (same average orientation of the core genes)
1. Normalized orientation (Gene orientation * Replicon orientation)
1. Gene size
1. MPF profile # if the gene is matched by macsyfinder, which profile matched ?
1. Integrase_type # if the gene is matched by tyrosine or serine recombinase profiles
1. Gene families # genes in other genome of the species that are homologous.  
1. RNA # RFAM hits, in the line of the closest gene, and formatted as: RNA_type&RFAM_number&start&end&strand

# References

If you use the script or the tutorial, please cite:

- Jean Cury, Marie Touchon, Eduardo P. C. Rocha, Integrative And Conjugative Elements And Their Hosts: Composition, Distribution, And Organization
bioRxiv 135814; doi: https://doi.org/10.1101/135814 (submitted to NAR)

- Abby Sophie S., Néron Bertrand, Ménager Hervé, Touchon Marie, Rocha Eduardo P. C. (2014). MacSyFinder: A Program to Mine Genomes for Molecular Systems with an Application to CRISPR-Cas Systems. In PLoS ONE, 9 (10), pp. e110726. [doi:10.1371/journal.pone.0110726](http://dx.doi.org/10.1371/journal.pone.0110726)

- Abby Sophie S., Cury Jean, Guglielmini Julien, Néron Bertrand, Touchon Marie, Rocha Eduardo P. C. (2016). Identification of protein secretion systems in bacterial genomes. In Scientific Reports, 6, pp. 23080. [doi:10.1038/srep23080](http://dx.doi.org/10.1038/srep23080)
